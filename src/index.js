import jQuery from 'jquery';
import Back from './back.js';

jQuery.noConflict();
var BackApp = (($, w) => {
  w._ = (...myVar) => console.log(...myVar);
  var APP = {
    init: () => {
      _('Main App initiated!!!');
      Back.init();
    },
  };
  $(document).ready( () => {
    APP.init();
  });
})(jQuery, window, undefined);